# arria10som-linux

This is the **[Dream Chip](http://www.dreamchip.de "Dream Chip Technologies GmbH") arria10som-linux** repository.

It contains modified linux sources based on Altera's socfpga-4.9.78-ltsi linux kernel ([Altera's Opensource linux repository](https://github.com/altera-opensource/linux-socfpga/tree/socfpga-4.9.78-ltsi)). ```(ACDS17.1_REL_GSRD_UPDATE1_PR)``` ```bdf910b1939e45beef022239bda381a2b11a6337 ```.

This linux version supports the **[Dream Chip Arria10 SoM](https://www.dreamchip.de/products/arria-10-system-on-module.html "The Arria 10 System on Module in detail")** with QSPI + eMMC or SD-Card

## Table of Contents

1. [Get the source code](#get_the_source)  
1.1. [Getting the latest version](#get_latest)  
1.2. [Getting a release version](#get_release)  
2. [Setup your toolchain](#setup_toolchain)  
3. [Build](#build)  
3.1. [Configure and compile the kernel](#build_config)  
3.2. [Compile the devicetree with QSPI & eMMC support](#build_dtb_qspi)  
3.3. [Compile the devicetree with SD-Card support](#build_dtb_sdmmc)
4. [Misc](#misc) 
## 1. Get the source code <a name="get_the_source"></a>

#### 1.1. Getting the latest version <a name="get_latest"></a>

    $ git clone https://gitlab.com/dreamchip/arria10som-linux.git

#### 1.2. Getting a release version <a name="get_release"></a>

    $ git clone https://gitlab.com/dreamchip/arria10som-linux.git -b dreamchip-arria10som-v1.0.0

## 2. Setup your toolchain <a name="setup_toolchain"></a>

    $ <path_to_toolchain>/intelFPGA/17.1/embedded/embedded_command_shell.sh
    $ export CROSS_COMPILE=arm-altera-eabi-
    $ export ARCH=arm

## 3. Build <a name="build"></a>

#### 3.1. Configure and compile the kernel <a name="build_config"></a>

    $ make dreamchip_arria10som_defconfig
    $ make zImage

#### 3.2. Compile the devicetree with QSPI & eMMC support <a name="build_dtb_qspi"></a>

	$ make dreamchip_arria10som_qspi.dtb
	
#### 3.3. Compile the devicetree with SD-Card support <a name="build_dtb_sdmmc"></a>

	$ make dreamchip_arria10som_sdmmc.dtb
## 4. Misc <a name="misc"></a>
*Please Note: The linux kernel could also be build via [Yocto](https://www.yoctoproject.org/ "The Yocto Project"). More information about Dream Chip's Yocto support could be found at the [arria10-manifest](https://gitlab.com/dreamchip/arria10som-manifest "Starting with Dream Chip's Yocot support") repository.*